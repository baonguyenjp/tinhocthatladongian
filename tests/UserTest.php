<?php

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testReturnsFullName()
    {
        require('User.php');

        $user = new User;

        $user->first_name = "Nguyen";
        $user->surname = "Bao";

        $this->assertEquals('Nguyen Bao', $user->getFullName());
    }
    public function testFullNameIsEmptyByDefault()
    {
        $user = new User;
        $this->assertEquals('', $user->getFullName());
    }

    /**
     * @test
     */
    public function user_has_first_name()
    {
        $user = new User;
        $user->first_name = "Bao";
        $this->assertEquals('Bao', $user->first_name);
    }
}